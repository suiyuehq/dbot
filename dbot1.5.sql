-- phpMyAdmin SQL Dump
-- version 4.4.15.10
-- https://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: 2022-04-19 17:32:00
-- 服务器版本： 5.6.50-log
-- PHP Version: 7.0.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dbot`
--

-- --------------------------------------------------------

--
-- 表的结构 `auth_group`
--

CREATE TABLE IF NOT EXISTS `auth_group` (
  `id` int(11) NOT NULL,
  `name` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `auth_group_permissions`
--

CREATE TABLE IF NOT EXISTS `auth_group_permissions` (
  `id` bigint(20) NOT NULL,
  `group_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `auth_permission`
--

CREATE TABLE IF NOT EXISTS `auth_permission` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `content_type_id` int(11) NOT NULL,
  `codename` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=57 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `auth_permission`
--

INSERT INTO `auth_permission` (`id`, `name`, `content_type_id`, `codename`) VALUES
(1, 'Can add log entry', 1, 'add_logentry'),
(2, 'Can change log entry', 1, 'change_logentry'),
(3, 'Can delete log entry', 1, 'delete_logentry'),
(4, 'Can view log entry', 1, 'view_logentry'),
(5, 'Can add permission', 2, 'add_permission'),
(6, 'Can change permission', 2, 'change_permission'),
(7, 'Can delete permission', 2, 'delete_permission'),
(8, 'Can view permission', 2, 'view_permission'),
(9, 'Can add group', 3, 'add_group'),
(10, 'Can change group', 3, 'change_group'),
(11, 'Can delete group', 3, 'delete_group'),
(12, 'Can view group', 3, 'view_group'),
(13, 'Can add user', 4, 'add_user'),
(14, 'Can change user', 4, 'change_user'),
(15, 'Can delete user', 4, 'delete_user'),
(16, 'Can view user', 4, 'view_user'),
(17, 'Can add content type', 5, 'add_contenttype'),
(18, 'Can change content type', 5, 'change_contenttype'),
(19, 'Can delete content type', 5, 'delete_contenttype'),
(20, 'Can view content type', 5, 'view_contenttype'),
(21, 'Can add session', 6, 'add_session'),
(22, 'Can change session', 6, 'change_session'),
(23, 'Can delete session', 6, 'delete_session'),
(24, 'Can view session', 6, 'view_session'),
(25, 'Can add job_detail', 7, 'add_job_detail'),
(26, 'Can change job_detail', 7, 'change_job_detail'),
(27, 'Can delete job_detail', 7, 'delete_job_detail'),
(28, 'Can view job_detail', 7, 'view_job_detail'),
(29, 'Can add ql', 8, 'add_ql'),
(30, 'Can change ql', 8, 'change_ql'),
(31, 'Can delete ql', 8, 'delete_ql'),
(32, 'Can view ql', 8, 'view_ql'),
(33, 'Can add export', 9, 'add_export'),
(34, 'Can change export', 9, 'change_export'),
(35, 'Can delete export', 9, 'delete_export'),
(36, 'Can view export', 9, 'view_export'),
(37, 'Can add words', 10, 'add_words'),
(38, 'Can change words', 10, 'change_words'),
(39, 'Can delete words', 10, 'delete_words'),
(40, 'Can view words', 10, 'view_words'),
(41, 'Can add cookie', 11, 'add_cookie'),
(42, 'Can change cookie', 11, 'change_cookie'),
(43, 'Can delete cookie', 11, 'delete_cookie'),
(44, 'Can view cookie', 11, 'view_cookie'),
(45, 'Can add mysql_config', 12, 'add_mysql_config'),
(46, 'Can change mysql_config', 12, 'change_mysql_config'),
(47, 'Can delete mysql_config', 12, 'delete_mysql_config'),
(48, 'Can view mysql_config', 12, 'view_mysql_config'),
(49, 'Can add users', 13, 'add_users'),
(50, 'Can change users', 13, 'change_users'),
(51, 'Can delete users', 13, 'delete_users'),
(52, 'Can view users', 13, 'view_users'),
(53, 'Can add user', 14, 'add_user'),
(54, 'Can change user', 14, 'change_user'),
(55, 'Can delete user', 14, 'delete_user'),
(56, 'Can view user', 14, 'view_user');

-- --------------------------------------------------------

--
-- 表的结构 `auth_user`
--

CREATE TABLE IF NOT EXISTS `auth_user` (
  `id` int(11) NOT NULL,
  `password` varchar(128) NOT NULL,
  `last_login` datetime(6) DEFAULT NULL,
  `is_superuser` tinyint(1) NOT NULL,
  `username` varchar(150) NOT NULL,
  `first_name` varchar(150) NOT NULL,
  `last_name` varchar(150) NOT NULL,
  `email` varchar(254) NOT NULL,
  `is_staff` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `date_joined` datetime(6) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `auth_user`
--

INSERT INTO `auth_user` (`id`, `password`, `last_login`, `is_superuser`, `username`, `first_name`, `last_name`, `email`, `is_staff`, `is_active`, `date_joined`) VALUES
(1, 'pbkdf2_sha256$260000$oLfPOLQH2gKQb7O9ntQRXD$vXPnG4wumOAo8BKtugP4PaH2IJ0ReAhksVHgz4AGotU=', '2022-04-19 16:22:32.328595', 1, 'admin', '', '', 'admin@qq.com', 1, 1, '2022-04-15 12:01:00.000000');

-- --------------------------------------------------------

--
-- 表的结构 `auth_user_groups`
--

CREATE TABLE IF NOT EXISTS `auth_user_groups` (
  `id` bigint(20) NOT NULL,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `auth_user_user_permissions`
--

CREATE TABLE IF NOT EXISTS `auth_user_user_permissions` (
  `id` bigint(20) NOT NULL,
  `user_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=53 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `auth_user_user_permissions`
--

INSERT INTO `auth_user_user_permissions` (`id`, `user_id`, `permission_id`) VALUES
(1, 1, 1),
(2, 1, 2),
(3, 1, 3),
(4, 1, 4),
(5, 1, 5),
(6, 1, 6),
(7, 1, 7),
(8, 1, 8),
(9, 1, 9),
(10, 1, 10),
(11, 1, 11),
(12, 1, 12),
(13, 1, 13),
(14, 1, 14),
(15, 1, 15),
(16, 1, 16),
(17, 1, 17),
(18, 1, 18),
(19, 1, 19),
(20, 1, 20),
(21, 1, 21),
(22, 1, 22),
(23, 1, 23),
(24, 1, 24),
(25, 1, 25),
(26, 1, 26),
(27, 1, 27),
(28, 1, 28),
(29, 1, 29),
(30, 1, 30),
(31, 1, 31),
(32, 1, 32),
(33, 1, 33),
(34, 1, 34),
(35, 1, 35),
(36, 1, 36),
(37, 1, 37),
(38, 1, 38),
(39, 1, 39),
(40, 1, 40),
(41, 1, 41),
(42, 1, 42),
(43, 1, 43),
(44, 1, 44),
(45, 1, 45),
(46, 1, 46),
(47, 1, 47),
(48, 1, 48),
(49, 1, 49),
(50, 1, 50),
(51, 1, 51),
(52, 1, 52);

-- --------------------------------------------------------

--
-- 表的结构 `dbot_config`
--

CREATE TABLE IF NOT EXISTS `dbot_config` (
  `id` int(11) NOT NULL,
  `type` longtext NOT NULL,
  `contents` longtext
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `dbot_config`
--

INSERT INTO `dbot_config` (`id`, `type`, `contents`) VALUES
(1, '当前版本', '2.3.5'),
(2, 'token', NULL);

-- --------------------------------------------------------

--
-- 表的结构 `dbot_cookie`
--

CREATE TABLE IF NOT EXISTS `dbot_cookie` (
  `id` int(11) NOT NULL,
  `qq` bigint(20) DEFAULT NULL,
  `cookie_type` varchar(64) NOT NULL COMMENT 'CK类型',
  `cookie` longtext NOT NULL COMMENT 'cookie'
) ENGINE=InnoDB AUTO_INCREMENT=89 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `dbot_cookie`
--

INSERT INTO `dbot_cookie` (`id`, `qq`, `cookie_type`, `cookie`) VALUES
(87, 2660867128, 'ksjsbCookie', '31231234123faesfawed'),
(88, 2660867128, '213', '123');

-- --------------------------------------------------------

--
-- 表的结构 `dbot_export`
--

CREATE TABLE IF NOT EXISTS `dbot_export` (
  `id` int(11) NOT NULL,
  `illustrate` varchar(128) NOT NULL COMMENT '说明',
  `export` varchar(128) NOT NULL COMMENT '类型变量',
  `up_down` enum('True','False') NOT NULL DEFAULT 'True' COMMENT '开关',
  `log` varchar(128) DEFAULT NULL COMMENT '日志文件夹名',
  `expired` varchar(64) DEFAULT NULL COMMENT '过期（关键字@数量）',
  `fen` varchar(12) NOT NULL DEFAULT '@' COMMENT '分割号',
  `up_key` varchar(1024) NOT NULL COMMENT '关键字上传（@）',
  `reply` varchar(1024) DEFAULT '默认用@隔开，已提交至 自选股 服务器\n结果：{msg}' COMMENT '回复',
  `max` bigint(20) NOT NULL DEFAULT '50' COMMENT '最大CK数量',
  `key_type` enum('保留','去除') NOT NULL DEFAULT '保留' COMMENT '关键字保留or去除',
  `up_type` enum('配置','变量') NOT NULL DEFAULT '配置' COMMENT 'CK存放位置'
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `dbot_export`
--

INSERT INTO `dbot_export` (`id`, `illustrate`, `export`, `up_down`, `log`, `expired`, `fen`, `up_key`, `reply`, `max`, `key_type`, `up_type`) VALUES
(1, '快手极速版', 'ksjsbCookie', 'True', 'ks_jsb', '6001@10', '@', 'kuaishou.api_st=@did=', '默认用@隔开，已提交至服务器\n结果：{msg}', 50, '保留', '配置'),
(2, '普通快手', 'ksCookie', 'True', 'raw_main_ks', NULL, '@', 'kuaishou.api_st=@did=', '默认用@隔开，已提交至服务器\n结果：{msg}', 50, '保留', '配置'),
(3, '自选股', 'TxStockCookie', 'True', 'raw_main_txstockV2', NULL, '@', 'openid@wzq_qlskey@wzq_qluin', '默认用@隔开，已提交至服务器\n结果：{msg}', 50, '保留', '配置'),
(4, '酷狗大字版', 'kugouurl', 'True', 'raw_main_kgdzb', NULL, '@', 'gateway.kugou.com/v1/incentive/user_info', '默认用@隔开，已提交至服务器\n结果：{msg}', 50, '保留', '配置'),
(5, '美团', 'mtTk', 'True', 'raw_MT_lb_meituan', NULL, '@', 'mt_c_token=', '默认用@隔开，已提交至服务器\n结果：{msg}', 50, '保留', '配置'),
(6, '饿了么', 'elmck', 'True', 'xiecoll_radish-script_lb_elm', NULL, '@', 'ubt_ssid=', '默认用@隔开，已提交至服务器\n结果：{msg}', 50, '保留', '配置'),
(8, '测试', 'test', 'True', '2660867128', '1', '@', 'test=', '默认用@隔开，已提交至 自选股 服务器结果：{msg}', 50, '保留', '变量');

-- --------------------------------------------------------

--
-- 表的结构 `dbot_job_detail`
--

CREATE TABLE IF NOT EXISTS `dbot_job_detail` (
  `id` bigint(20) NOT NULL,
  `CREATED_BY` varchar(32) NOT NULL,
  `CREATED_TIME` datetime(6) NOT NULL,
  `UPDATED_BY` varchar(32) NOT NULL,
  `UPDATED_TIME` datetime(6) NOT NULL,
  `JOB_TYPE` varchar(32) NOT NULL,
  `JOB_NAME` varchar(128) NOT NULL,
  `JOB_COMMENT` varchar(128) NOT NULL,
  `IN_PARA` varchar(32) NOT NULL,
  `IN_PARA_COMMENT` varchar(1024) NOT NULL,
  `OUT_PARA` varchar(32) NOT NULL,
  `OUT_PARA_COMMENT` varchar(1024) NOT NULL,
  `VERSION` varchar(32) NOT NULL,
  `IS_DELETE` varchar(1) NOT NULL,
  `PRO_STATUS` varchar(1) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `dbot_job_detail`
--

INSERT INTO `dbot_job_detail` (`id`, `CREATED_BY`, `CREATED_TIME`, `UPDATED_BY`, `UPDATED_TIME`, `JOB_TYPE`, `JOB_NAME`, `JOB_COMMENT`, `IN_PARA`, `IN_PARA_COMMENT`, `OUT_PARA`, `OUT_PARA_COMMENT`, `VERSION`, `IS_DELETE`, `PRO_STATUS`) VALUES
(1, '', '2022-04-15 17:05:04.508452', '', '2022-04-15 17:05:04.508452', '02', '123', '123', '123', '', '123', '', '', 'N', 'N');

-- --------------------------------------------------------

--
-- 表的结构 `dbot_ql`
--

CREATE TABLE IF NOT EXISTS `dbot_ql` (
  `id` int(11) NOT NULL,
  `url` varchar(128) NOT NULL,
  `Client_ID` varchar(64) NOT NULL,
  `Client_Secret` varchar(128) NOT NULL,
  `token` varchar(128) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `dbot_ql`
--

INSERT INTO `dbot_ql` (`id`, `url`, `Client_ID`, `Client_Secret`, `token`) VALUES
(1, '1', '1', '1', '1');

-- --------------------------------------------------------

--
-- 表的结构 `dbot_user`
--

CREATE TABLE IF NOT EXISTS `dbot_user` (
  `id` int(11) NOT NULL,
  `qq` varchar(32) NOT NULL,
  `admin` varchar(32) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `dbot_user`
--

INSERT INTO `dbot_user` (`id`, `qq`, `admin`) VALUES
(1, '2660867128', '1');

-- --------------------------------------------------------

--
-- 表的结构 `dbot_words`
--

CREATE TABLE IF NOT EXISTS `dbot_words` (
  `id` int(11) NOT NULL,
  `en_type` varchar(128) NOT NULL COMMENT '查询类型',
  `word` varchar(1024) NOT NULL COMMENT '问',
  `directions` varchar(1024) DEFAULT NULL,
  `reply` varchar(1024) DEFAULT NULL,
  `find_type` enum('精确','模糊') NOT NULL DEFAULT '精确' COMMENT '精确模糊查找',
  `word_type` enum('命令','问答','禁言','撤回','禁撤','定时') NOT NULL DEFAULT '问答' COMMENT '类型',
  `type_contents` varchar(128) DEFAULT NULL,
  `permission` enum('管理员','用户') NOT NULL DEFAULT '用户' COMMENT '使用权限'
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `dbot_words`
--

INSERT INTO `dbot_words` (`id`, `en_type`, `word`, `directions`, `reply`, `find_type`, `word_type`, `type_contents`, `permission`) VALUES
(1, 'add_export', '增加账号1', '增加export xxx的命令，分割号指多账户分割的符号\n例子：增加账号 分割号 xxx 增加的ck', '{msg}', '精确', '命令', NULL, '管理员'),
(2, 'del_export', '删除其它账号', '删除export xxx的命令\n例子：删除其它账号 xxx 删除的ck/账户n', '{msg}', '精确', '命令', NULL, '管理员'),
(3, 'find_export', '其它查询', '查询export xxx的命令\n例子：其它查询 xxx', '{msg}', '精确', '命令', NULL, '管理员'),
(4, 'find_ks_log', '查询快手日志', '例子：查询快手日志 快手名', '{msg}', '精确', '命令', NULL, '管理员'),
(5, 'find_ks_info', '查询快手', '例子：查询快手 快手名\n查询快手余额', '账号【日志号{log_num}】：{jsb_name}余额：{jsb_rmb}金币：{jsb_jinbi}@账号【日志号{log_num}】：{ks_name}余额：{ks_rmb}金币：{ks_jinbi}', '精确', '命令', NULL, '用户'),
(6, 'find_ks_user', '查询快手过期账号', '查询快手过期账号', '根据运行日志进行查询，并不是实时查询，例如你删除一个快手账号，再次查询还是这些结果，需要再运行一下快手，等运行完毕日志生成后即可更新这个查询过期数量：{guoqi_num}@账号【日志号{log_num}】：{name}', '精确', '命令', NULL, '管理员'),
(7, 'set_config', '设置配置', '例子：设置配置 config 内容', '{msg}', '精确', '命令', NULL, '管理员'),
(8, 'del_ks', '删除快手', '例子：删除快手 QQ 快手名', '{msg}', '精确', '命令', NULL, '管理员'),
(9, 'find_zixuangu', '查询自选股', '例子：查询自选股 账户名称', '账户：{zixuangu_name}金币：{zixuangu_jinbi}{zixuangu_1}{zixuangu_2}{zixuangu_3}{zixuangu_4}', '精确', '命令', NULL, '用户'),
(10, 'add_admin', '增加管理员', '例子：增加管理员 QQ', '{msg}', '精确', '命令', NULL, '管理员'),
(11, 'del_admin', '删除管理员', '例子：删除管理员 QQ', '{msg}', '精确', '命令', NULL, '管理员'),
(12, 'menu', '菜单', NULL, '#类型为config.sh里面的export xxx=””的xxx，只要你config.sh里面有什么类型，而且都是用@分割的多账号，都可以使用这条命令\n增加账号 [类型] [CK]\n\n其它查询 [类型]\n#输出账号数量，和每个账号N\n\n删除其它账号 [类型] [CK]/[账号n]\n#账号n对应查询的账号n\n\n查询快手日志 [快手账户名]\n查询快手 [快手账户名]\n查询快手过期账号', '精确', '命令', NULL, '用户'),
(13, 'find_mt_user', '查询美团', '例子：查询美团 账号名', '账号：{name}\n{mili}\n{jinbi}\n{juan}', '精确', '命令', NULL, '用户'),
(14, 'find_elm_user', '查询饿了么', '例子：查询饿了么 账号名', '账号：{name}\n{ci_dou}\n{juan}', '精确', '命令', NULL, '用户');

-- --------------------------------------------------------

--
-- 表的结构 `django_admin_log`
--

CREATE TABLE IF NOT EXISTS `django_admin_log` (
  `id` int(11) NOT NULL,
  `action_time` datetime(6) NOT NULL,
  `object_id` longtext,
  `object_repr` varchar(200) NOT NULL,
  `action_flag` smallint(5) unsigned NOT NULL,
  `change_message` longtext NOT NULL,
  `content_type_id` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `django_admin_log`
--

INSERT INTO `django_admin_log` (`id`, `action_time`, `object_id`, `object_repr`, `action_flag`, `change_message`, `content_type_id`, `user_id`) VALUES
(1, '2022-04-15 17:05:04.544075', '1', 'job_detail object (1)', 1, '[{"added": {}}]', 7, 1),
(2, '2022-04-15 18:04:47.221742', '1', 'words object (1)', 2, '[{"changed": {"fields": ["\\u95ee"]}}]', 10, 1),
(3, '2022-04-15 18:04:47.282999', '5', 'words object (5)', 2, '[{"changed": {"fields": ["\\u7b54"]}}]', 10, 1),
(4, '2022-04-15 18:04:47.347390', '6', 'words object (6)', 2, '[{"changed": {"fields": ["\\u7b54"]}}]', 10, 1),
(5, '2022-04-15 18:04:47.411111', '9', 'words object (9)', 2, '[{"changed": {"fields": ["\\u7b54"]}}]', 10, 1),
(6, '2022-04-15 18:59:35.850626', '87', 'cookie object (87)', 1, '[{"added": {}}]', 11, 1),
(7, '2022-04-16 11:15:55.338915', '1', 'admin', 2, '[{"changed": {"fields": ["User permissions", "Last login"]}}]', 4, 1),
(8, '2022-04-18 22:01:46.947657', '7', 'export object (7)', 1, '[{"added": {}}]', 9, 1),
(9, '2022-04-18 22:02:00.351825', '7', 'export object (7)', 3, '', 9, 1),
(10, '2022-04-18 22:14:46.118398', '88', 'cookie object (88)', 1, '[{"added": {}}]', 11, 1),
(11, '2022-04-19 16:31:54.976912', '8', 'export object (8)', 1, '[{"added": {}}]', 9, 1);

-- --------------------------------------------------------

--
-- 表的结构 `django_content_type`
--

CREATE TABLE IF NOT EXISTS `django_content_type` (
  `id` int(11) NOT NULL,
  `app_label` varchar(100) NOT NULL,
  `model` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `django_content_type`
--

INSERT INTO `django_content_type` (`id`, `app_label`, `model`) VALUES
(1, 'admin', 'logentry'),
(3, 'auth', 'group'),
(2, 'auth', 'permission'),
(4, 'auth', 'user'),
(5, 'contenttypes', 'contenttype'),
(11, 'dbot', 'cookie'),
(9, 'dbot', 'export'),
(7, 'dbot', 'job_detail'),
(12, 'dbot', 'mysql_config'),
(8, 'dbot', 'ql'),
(14, 'dbot', 'user'),
(13, 'dbot', 'users'),
(10, 'dbot', 'words'),
(6, 'sessions', 'session');

-- --------------------------------------------------------

--
-- 表的结构 `django_migrations`
--

CREATE TABLE IF NOT EXISTS `django_migrations` (
  `id` bigint(20) NOT NULL,
  `app` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `applied` datetime(6) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `django_migrations`
--

INSERT INTO `django_migrations` (`id`, `app`, `name`, `applied`) VALUES
(1, 'contenttypes', '0001_initial', '2022-04-15 11:57:01.262185'),
(2, 'auth', '0001_initial', '2022-04-15 11:57:02.947192'),
(3, 'admin', '0001_initial', '2022-04-15 11:57:03.399664'),
(4, 'admin', '0002_logentry_remove_auto_add', '2022-04-15 11:57:03.482959'),
(5, 'admin', '0003_logentry_add_action_flag_choices', '2022-04-15 11:57:03.566503'),
(6, 'contenttypes', '0002_remove_content_type_name', '2022-04-15 11:57:03.914675'),
(7, 'auth', '0002_alter_permission_name_max_length', '2022-04-15 11:57:04.088770'),
(8, 'auth', '0003_alter_user_email_max_length', '2022-04-15 11:57:04.284111'),
(9, 'auth', '0004_alter_user_username_opts', '2022-04-15 11:57:04.363602'),
(10, 'auth', '0005_alter_user_last_login_null', '2022-04-15 11:57:04.569545'),
(11, 'auth', '0006_require_contenttypes_0002', '2022-04-15 11:57:04.643629'),
(12, 'auth', '0007_alter_validators_add_error_messages', '2022-04-15 11:57:04.720725'),
(13, 'auth', '0008_alter_user_username_max_length', '2022-04-15 11:57:04.906677'),
(14, 'auth', '0009_alter_user_last_name_max_length', '2022-04-15 11:57:05.088095'),
(15, 'auth', '0010_alter_group_name_max_length', '2022-04-15 11:57:05.256947'),
(16, 'auth', '0011_update_proxy_permissions', '2022-04-15 11:57:05.442825'),
(17, 'auth', '0012_alter_user_first_name_max_length', '2022-04-15 11:57:05.630202'),
(18, 'dbot', '0001_initial', '2022-04-15 11:57:05.766888'),
(19, 'dbot', '0002_alter_job_detail_in_para', '2022-04-15 11:57:05.842239'),
(20, 'sessions', '0001_initial', '2022-04-15 11:57:06.061556'),
(21, 'dbot', '0003_ql', '2022-04-15 13:19:21.542849'),
(22, 'dbot', '0004_ql_token_alter_ql_client_id_alter_ql_client_secret_and_more', '2022-04-15 13:46:17.508550'),
(23, 'dbot', '0005_alter_ql_token', '2022-04-15 13:46:17.579612'),
(24, 'dbot', '0006_alter_ql_client_id', '2022-04-15 13:46:17.648582'),
(25, 'dbot', '0007_alter_ql_token_alter_ql_url', '2022-04-15 13:46:17.723751'),
(26, 'dbot', '0008_export_alter_ql_token', '2022-04-15 16:52:15.711524'),
(27, 'dbot', '0009_words_remove_export_replay_export_reply_and_more', '2022-04-15 17:53:02.265668'),
(28, 'dbot', '0010_cookie', '2022-04-15 18:58:23.023755'),
(29, 'dbot', '0011_mysql_config', '2022-04-15 20:47:01.510705'),
(30, 'dbot', '0012_users', '2022-04-15 23:09:53.136833'),
(31, 'dbot', '0013_rename_users_user', '2022-04-15 23:17:12.721698'),
(32, 'dbot', '0014_rename_mysql_config_config_alter_user_id', '2022-04-16 11:20:20.278962');

-- --------------------------------------------------------

--
-- 表的结构 `django_session`
--

CREATE TABLE IF NOT EXISTS `django_session` (
  `session_key` varchar(40) NOT NULL,
  `session_data` longtext NOT NULL,
  `expire_date` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `django_session`
--

INSERT INTO `django_session` (`session_key`, `session_data`, `expire_date`) VALUES
('0bqsr9l7cfhdkqms4gx27jwdfdsnwu8k', '.eJyNU8FuozAQ_ZWIc0iwAQM9tt3DalX1A5YqsvE4uAXMYqN2VeXflwndNoRE9DRm5vHePHvm3dvx3pW73kK309K78Yi3Ps0JXrxAgwX5zJu92RSmcZ0WG4RsPqp282AkVLcf2AlByW05_E1DGkogDKQoQCVBIGiqQMZxUCSECyUVcEaIzAhhKZMxD0NGBKcgaBKq4khaQ9Pbgev3e-7xts29m1Xu1X-Px_VwbHgNYzLP-6RQAgNIhUFwgiEK2IjVg48RC5WPH771nTGVHcs1-rEIQLEJcZbEFIPKsiGkQgASKwZXicE53ezHct9VY1UK47Z_qu2YBi0xTYKAHp4wsXsFvS8dJoMpgBzWq0lP97fGfbMTv4SqXXQYhzJFh6RQl3inFuCtNZ3bXpdsO1Mbp7EwNRKdG0HpKJFDiACiZelX00l7VVlyB-eS8bnk3ePjr58_Lg3IVKow5kXDVa1hBKuZGLvgb5gdvFpGw2RZFPdnJqn4SnEfS-d6yWxyyBQQjg19rg4u6YXNYUkU4vOzODi-AR-foiDzVuz_Xnxbaqjk4mid-l8w9nkX2ObpXXwZymaO6RSQHp68wz_3y3_6:1ngjjp:0-9hTum2D1b9j3PWbADhbMshlpC7flR9S-VbfnMFrLw', '2022-04-19 18:01:33.294782'),
('0iiyzald0z40xlbsmzg33wef1tn2f25c', '.eJyNU9tymzAQ_RUPz8ZGgATkMW0fOp1OPqBkPLqsjBpAFIlJOxn_e9mQJsbYQ59W7Dns2SPtvgQHPvjqMDjoD0YFdwEJtuc5weUTtAion7w92p20re-N2CFl94a63XeroL5_484KVNxV499UKcijNOIMCiKKQsaUaZbGNIckE4nKdC5iKqkuckKZBEXTEdE5UBVxQikWbaAd3Fjrx0sZ8K4rg7tNGTR_Xo_b8djyBqZkWQ6Z1AIDKI1BcIIhjdjENaOPiQt1iB-hC721tZvgBv04JKDYrHCR0RiDLoox5EIAFtYMbhYG7017nOChrydUCev3v-r9lAajME2iKD49YuLwDOZYeUxGcwI5bTeznj7fW_-fnYQV1N2qQ5qoHB0Sqa_VnVuA353t_f62ZNfbxnqDwNxIemkEpdNMjSEFSNeln22v3E1lxT1cStJLyU8PD9--frk2IHMpae2TgZta4wjWCzF2xd84O3i1LE6ydVHcn4Wk5hvNQ4Qu9bLF5JA5IZkael8dXNIrm8OyNMHnZzR6fQM-PYUky1bcv15CVxmo1eponftfMfZ-F9jm-V18GCoWjuM5IT89Bqe_P8B-5Q:1nffU8:4UbwagnlA3AE_RswkFoMAxHgN95jc0oD9AaW0vcDp_E', '2022-04-16 19:16:56.319733'),
('1zrzr35cnqj9wtb7t3wy7sefj5c3z0lt', '.eJyNU9tymzAQ_RUPz8ZGgATkMW0fOp1OPqBkPLqsjBpAFIlJOxn_e9mQJsbYQ59W7Dns2SPtvgQHPvjqMDjoD0YFdwEJtuc5weUTtAion7w92p20re-N2CFl94a63XeroL5_484KVNxV499UKcijNOIMCiKKQsaUaZbGNIckE4nKdC5iKqkuckKZBEXTEdE5UBVxQikWbaAd3Fjrx0sZ8K4rg7tNGTR_Xo_b8djyBqZkWQ6Z1AIDKI1BcIIhjdjENaOPiQt1iB-hC721tZvgBv04JKDYrHCR0RiDLoox5EIAFtYMbhYG7017nOChrydUCev3v-r9lAajME2iKD49YuLwDOZYeUxGcwI5bTeznj7fW_-fnYQV1N2qQ5qoHB0Sqa_VnVuA353t_f62ZNfbxnqDwNxIemkEpdNMjSEFSNeln22v3E1lxT1cStJLyU8PD9--frk2IHMpae2TgZta4wjWCzF2xd84O3i1LE6ydVHcn4Wk5hvNQ4Qu9bLF5JA5IZkael8dXNIrm8OyNMHnZzR6fQM-PYUky1bcv15CVxmo1eponftfMfZ-F9jm-V18GCoWjuM5IT89Bqe_P8B-5Q:1nfuYm:b3g2e3KuBMCKTIDK7Sdoh0u5A50kax_VTQ_JDO_Ijyw', '2022-04-17 11:22:44.913533'),
('2gomo4ylg48gx8m5y7dwrpgac742wevn', '.eJyNU9tymzAQ_RUPz8ZGgATkMW0fOp1OPqBkPLqsjBpAFIlJOxn_e9mQJsbYQ59W7Dns2SPtvgQHPvjqMDjoD0YFdwEJtuc5weUTtAion7w92p20re-N2CFl94a63XeroL5_484KVNxV499UKcijNOIMCiKKQsaUaZbGNIckE4nKdC5iKqkuckKZBEXTEdE5UBVxQikWbaAd3Fjrx0sZ8K4rg7tNGTR_Xo_b8djyBqZkWQ6Z1AIDKI1BcIIhjdjENaOPiQt1iB-hC721tZvgBv04JKDYrHCR0RiDLoox5EIAFtYMbhYG7017nOChrydUCev3v-r9lAajME2iKD49YuLwDOZYeUxGcwI5bTeznj7fW_-fnYQV1N2qQ5qoHB0Sqa_VnVuA353t_f62ZNfbxnqDwNxIemkEpdNMjSEFSNeln22v3E1lxT1cStJLyU8PD9--frk2IHMpae2TgZta4wjWCzF2xd84O3i1LE6ydVHcn4Wk5hvNQ4Qu9bLF5JA5IZkael8dXNIrm8OyNMHnZzR6fQM-PYUky1bcv15CVxmo1eponftfMfZ-F9jm-V18GCoWjuM5IT89Bqe_P8B-5Q:1nfjGV:40qkMag19Qap69U7QYn8oTtK32DF90zlOpqUMXX3vd8', '2022-04-16 23:19:07.918516'),
('3i8o8yfnw2aavggc8agk5we2w9lhu7d5', '.eJyNU9tymzAQ_RUPz8ZGgATkMW0fOp1OPqBkPLqsjBpAFIlJOxn_e9mQJsbYQ59W7Dns2SPtvgQHPvjqMDjoD0YFdwEJtuc5weUTtAion7w92p20re-N2CFl94a63XeroL5_484KVNxV499UKcijNOIMCiKKQsaUaZbGNIckE4nKdC5iKqkuckKZBEXTEdE5UBVxQikWbaAd3Fjrx0sZ8K4rg7tNGTR_Xo_b8djyBqZkWQ6Z1AIDKI1BcIIhjdjENaOPiQt1iB-hC721tZvgBv04JKDYrHCR0RiDLoox5EIAFtYMbhYG7017nOChrydUCev3v-r9lAajME2iKD49YuLwDOZYeUxGcwI5bTeznj7fW_-fnYQV1N2qQ5qoHB0Sqa_VnVuA353t_f62ZNfbxnqDwNxIemkEpdNMjSEFSNeln22v3E1lxT1cStJLyU8PD9--frk2IHMpae2TgZta4wjWCzF2xd84O3i1LE6ydVHcn4Wk5hvNQ4Qu9bLF5JA5IZkael8dXNIrm8OyNMHnZzR6fQM-PYUky1bcv15CVxmo1eponftfMfZ-F9jm-V18GCoWjuM5IT89Bqe_P8B-5Q:1ngIr3:NcfdMektJbeOXBL2cM2iqxbZkJz1Rz6lTc-ym6UfPxs', '2022-04-18 13:19:13.765159'),
('3nqn2lgab7epybdfuml4j9nk97s7y9rt', '.eJyNU9tymzAQ_RUPz8ZGgATkMW0fOp1OPqBkPLqsjBpAFIlJOxn_e9mQJsbYQ59W7Dns2SPtvgQHPvjqMDjoD0YFdwEJtuc5weUTtAion7w92p20re-N2CFl94a63XeroL5_484KVNxV499UKcijNOIMCiKKQsaUaZbGNIckE4nKdC5iKqkuckKZBEXTEdE5UBVxQikWbaAd3Fjrx0sZ8K4rg7tNGTR_Xo_b8djyBqZkWQ6Z1AIDKI1BcIIhjdjENaOPiQt1iB-hC721tZvgBv04JKDYrHCR0RiDLoox5EIAFtYMbhYG7017nOChrydUCev3v-r9lAajME2iKD49YuLwDOZYeUxGcwI5bTeznj7fW_-fnYQV1N2qQ5qoHB0Sqa_VnVuA353t_f62ZNfbxnqDwNxIemkEpdNMjSEFSNeln22v3E1lxT1cStJLyU8PD9--frk2IHMpae2TgZta4wjWCzF2xd84O3i1LE6ydVHcn4Wk5hvNQ4Qu9bLF5JA5IZkael8dXNIrm8OyNMHnZzR6fQM-PYUky1bcv15CVxmo1eponftfMfZ-F9jm-V18GCoWjuM5IT89Bqe_P8B-5Q:1ngeNE:SR08b-BOqrAQfrLiZm5Bgq4MYE9yl0X6AcO3njTHuEs', '2022-04-19 12:17:52.713380'),
('cbp87j1dyq18chh4cow6gmjgcb13xpdf', '.eJyNU9tymzAQ_RUPz8ZGgATkMW0fOp1OPqBkPLqsjBpAFIlJOxn_e9mQJsbYQ59W7Dns2SPtvgQHPvjqMDjoD0YFdwEJtuc5weUTtAion7w92p20re-N2CFl94a63XeroL5_484KVNxV499UKcijNOIMCiKKQsaUaZbGNIckE4nKdC5iKqkuckKZBEXTEdE5UBVxQikWbaAd3Fjrx0sZ8K4rg7tNGTR_Xo_b8djyBqZkWQ6Z1AIDKI1BcIIhjdjENaOPiQt1iB-hC721tZvgBv04JKDYrHCR0RiDLoox5EIAFtYMbhYG7017nOChrydUCev3v-r9lAajME2iKD49YuLwDOZYeUxGcwI5bTeznj7fW_-fnYQV1N2qQ5qoHB0Sqa_VnVuA353t_f62ZNfbxnqDwNxIemkEpdNMjSEFSNeln22v3E1lxT1cStJLyU8PD9--frk2IHMpae2TgZta4wjWCzF2xd84O3i1LE6ydVHcn4Wk5hvNQ4Qu9bLF5JA5IZkael8dXNIrm8OyNMHnZzR6fQM-PYUky1bcv15CVxmo1eponftfMfZ-F9jm-V18GCoWjuM5IT89Bqe_P8B-5Q:1nfegI:fM4TQ_YcSyWylVEMPNh7KrnZphJMqeBu0eR0juVenWA', '2022-04-16 18:25:26.237489'),
('f8eb8a9d4h16u7r3crzgr6xoselqy8xr', '.eJyNU9tymzAQ_RUPz8ZGgATkMW0fOp1OPqBkPLqsjBpAFIlJOxn_e9mQJsbYQ59W7Dns2SPtvgQHPvjqMDjoD0YFdwEJtuc5weUTtAion7w92p20re-N2CFl94a63XeroL5_484KVNxV499UKcijNOIMCiKKQsaUaZbGNIckE4nKdC5iKqkuckKZBEXTEdE5UBVxQikWbaAd3Fjrx0sZ8K4rg7tNGTR_Xo_b8djyBqZkWQ6Z1AIDKI1BcIIhjdjENaOPiQt1iB-hC721tZvgBv04JKDYrHCR0RiDLoox5EIAFtYMbhYG7017nOChrydUCev3v-r9lAajME2iKD49YuLwDOZYeUxGcwI5bTeznj7fW_-fnYQV1N2qQ5qoHB0Sqa_VnVuA353t_f62ZNfbxnqDwNxIemkEpdNMjSEFSNeln22v3E1lxT1cStJLyU8PD9--frk2IHMpae2TgZta4wjWCzF2xd84O3i1LE6ydVHcn4Wk5hvNQ4Qu9bLF5JA5IZkael8dXNIrm8OyNMHnZzR6fQM-PYUky1bcv15CVxmo1eponftfMfZ-F9jm-V18GCoWjuM5IT89Bqe_P8B-5Q:1nfgCB:NJLmpQPOyeUs0DJM-Glvc0uqNhXesrDWlvkeVPwsPpc', '2022-04-16 20:02:27.594430'),
('gq2jc6al2p8f54sbt09063698cko0x2b', '.eJyNU9tymzAQ_RUPz8ZGgATkMW0fOp1OPqBkPLqsjBpAFIlJOxn_e9mQJsbYQ59W7Dns2SPtvgQHPvjqMDjoD0YFdwEJtuc5weUTtAion7w92p20re-N2CFl94a63XeroL5_484KVNxV499UKcijNOIMCiKKQsaUaZbGNIckE4nKdC5iKqkuckKZBEXTEdE5UBVxQikWbaAd3Fjrx0sZ8K4rg7tNGTR_Xo_b8djyBqZkWQ6Z1AIDKI1BcIIhjdjENaOPiQt1iB-hC721tZvgBv04JKDYrHCR0RiDLoox5EIAFtYMbhYG7017nOChrydUCev3v-r9lAajME2iKD49YuLwDOZYeUxGcwI5bTeznj7fW_-fnYQV1N2qQ5qoHB0Sqa_VnVuA353t_f62ZNfbxnqDwNxIemkEpdNMjSEFSNeln22v3E1lxT1cStJLyU8PD9--frk2IHMpae2TgZta4wjWCzF2xd84O3i1LE6ydVHcn4Wk5hvNQ4Qu9bLF5JA5IZkael8dXNIrm8OyNMHnZzR6fQM-PYUky1bcv15CVxmo1eponftfMfZ-F9jm-V18GCoWjuM5IT89Bqe_P8B-5Q:1nghKU:OkAh4hq5GGa1IQYt1CTbxu63W3UrFyNSUSx3DAB8SWk', '2022-04-19 15:27:14.921249'),
('m3laiqw7yk437uplhf2ptxs16dmntt95', '.eJyNU9tymzAQ_RUPz8ZGgATkMW0fOp1OPqBkPLqsjBpAFIlJOxn_e9mQJsbYQ59W7Dns2SPtvgQHPvjqMDjoD0YFdwEJtuc5weUTtAion7w92p20re-N2CFl94a63XeroL5_484KVNxV499UKcijNOIMCiKKQsaUaZbGNIckE4nKdC5iKqkuckKZBEXTEdE5UBVxQikWbaAd3Fjrx0sZ8K4rg7tNGTR_Xo_b8djyBqZkWQ6Z1AIDKI1BcIIhjdjENaOPiQt1iB-hC721tZvgBv04JKDYrHCR0RiDLoox5EIAFtYMbhYG7017nOChrydUCev3v-r9lAajME2iKD49YuLwDOZYeUxGcwI5bTeznj7fW_-fnYQV1N2qQ5qoHB0Sqa_VnVuA353t_f62ZNfbxnqDwNxIemkEpdNMjSEFSNeln22v3E1lxT1cStJLyU8PD9--frk2IHMpae2TgZta4wjWCzF2xd84O3i1LE6ydVHcn4Wk5hvNQ4Qu9bLF5JA5IZkael8dXNIrm8OyNMHnZzR6fQM-PYUky1bcv15CVxmo1eponftfMfZ-F9jm-V18GCoWjuM5IT89Bqe_P8B-5Q:1ng5mv:okmtoWkBkT2GXweSX-Xvp8LYnxFy4Nj97p1xLpzcKhg', '2022-04-17 23:22:05.321451'),
('nkkmmgwvkugucd5wnb99gzaamj5s0fvb', '.eJyNU9tymzAQ_RUPz8ZGgATkMW0fOp1OPqBkPLqsjBpAFIlJOxn_e9mQJsbYQ59W7Dns2SPtvgQHPvjqMDjoD0YFdwEJtuc5weUTtAion7w92p20re-N2CFl94a63XeroL5_484KVNxV499UKcijNOIMCiKKQsaUaZbGNIckE4nKdC5iKqkuckKZBEXTEdE5UBVxQikWbaAd3Fjrx0sZ8K4rg7tNGTR_Xo_b8djyBqZkWQ6Z1AIDKI1BcIIhjdjENaOPiQt1iB-hC721tZvgBv04JKDYrHCR0RiDLoox5EIAFtYMbhYG7017nOChrydUCev3v-r9lAajME2iKD49YuLwDOZYeUxGcwI5bTeznj7fW_-fnYQV1N2qQ5qoHB0Sqa_VnVuA353t_f62ZNfbxnqDwNxIemkEpdNMjSEFSNeln22v3E1lxT1cStJLyU8PD9--frk2IHMpae2TgZta4wjWCzF2xd84O3i1LE6ydVHcn4Wk5hvNQ4Qu9bLF5JA5IZkael8dXNIrm8OyNMHnZzR6fQM-PYUky1bcv15CVxmo1eponftfMfZ-F9jm-V18GCoWjuM5IT89Bqe_P8B-5Q:1ngSPs:2xO6wR_sevmL-5kgIiLEdc2kaPxtYD8R_ydEWYUZ7Ik', '2022-04-18 23:31:48.742212'),
('nw30gdq72fv6hu5aybaeeu9n594iikxd', '.eJyNU9tymzAQ_RUPz8ZGgATkMW0fOp1OPqBkPLqsjBpAFIlJOxn_e9mQJsbYQ59W7Dns2SPtvgQHPvjqMDjoD0YFdwEJtuc5weUTtAion7w92p20re-N2CFl94a63XeroL5_484KVNxV499UKcijNOIMCiKKQsaUaZbGNIckE4nKdC5iKqkuckKZBEXTEdE5UBVxQikWbaAd3Fjrx0sZ8K4rg7tNGTR_Xo_b8djyBqZkWQ6Z1AIDKI1BcIIhjdjENaOPiQt1iB-hC721tZvgBv04JKDYrHCR0RiDLoox5EIAFtYMbhYG7017nOChrydUCev3v-r9lAajME2iKD49YuLwDOZYeUxGcwI5bTeznj7fW_-fnYQV1N2qQ5qoHB0Sqa_VnVuA353t_f62ZNfbxnqDwNxIemkEpdNMjSEFSNeln22v3E1lxT1cStJLyU8PD9--frk2IHMpae2TgZta4wjWCzF2xd84O3i1LE6ydVHcn4Wk5hvNQ4Qu9bLF5JA5IZkael8dXNIrm8OyNMHnZzR6fQM-PYUky1bcv15CVxmo1eponftfMfZ-F9jm-V18GCoWjuM5IT89Bqe_P8B-5Q:1ngHxL:TEp5ep7YFmP1XjAkYUSNW5BsjIQVdbBRDSIFcuBqd_0', '2022-04-18 12:21:39.649236'),
('rfhk54yzxzurark03ah5him623s5o6u3', '.eJyNU9tymzAQ_RUPz8ZGgATkMW0fOp1OPqBkPLqsjBpAFIlJOxn_e9mQJsbYQ59W7Dns2SPtvgQHPvjqMDjoD0YFdwEJtuc5weUTtAion7w92p20re-N2CFl94a63XeroL5_484KVNxV499UKcijNOIMCiKKQsaUaZbGNIckE4nKdC5iKqkuckKZBEXTEdE5UBVxQikWbaAd3Fjrx0sZ8K4rg7tNGTR_Xo_b8djyBqZkWQ6Z1AIDKI1BcIIhjdjENaOPiQt1iB-hC721tZvgBv04JKDYrHCR0RiDLoox5EIAFtYMbhYG7017nOChrydUCev3v-r9lAajME2iKD49YuLwDOZYeUxGcwI5bTeznj7fW_-fnYQV1N2qQ5qoHB0Sqa_VnVuA353t_f62ZNfbxnqDwNxIemkEpdNMjSEFSNeln22v3E1lxT1cStJLyU8PD9--frk2IHMpae2TgZta4wjWCzF2xd84O3i1LE6ydVHcn4Wk5hvNQ4Qu9bLF5JA5IZkael8dXNIrm8OyNMHnZzR6fQM-PYUky1bcv15CVxmo1eponftfMfZ-F9jm-V18GCoWjuM5IT89Bqe_P8B-5Q:1nfavE:UWD5XCDAZW1mqZmAPIDUknInLFFhLap9Va9QFPzWgXQ', '2022-04-16 14:24:36.339996'),
('wgwjun2tje8k0xorpwpn34unnrx67lfj', '.eJyNU9tymzAQ_RUPz8ZGgATkMW0fOp1OPqBkPLqsjBpAFIlJOxn_e9mQJsbYQ59W7Dns2SPtvgQHPvjqMDjoD0YFdwEJtuc5weUTtAion7w92p20re-N2CFl94a63XeroL5_484KVNxV499UKcijNOIMCiKKQsaUaZbGNIckE4nKdC5iKqkuckKZBEXTEdE5UBVxQikWbaAd3Fjrx0sZ8K4rg7tNGTR_Xo_b8djyBqZkWQ6Z1AIDKI1BcIIhjdjENaOPiQt1iB-hC721tZvgBv04JKDYrHCR0RiDLoox5EIAFtYMbhYG7017nOChrydUCev3v-r9lAajME2iKD49YuLwDOZYeUxGcwI5bTeznj7fW_-fnYQV1N2qQ5qoHB0Sqa_VnVuA353t_f62ZNfbxnqDwNxIemkEpdNMjSEFSNeln22v3E1lxT1cStJLyU8PD9--frk2IHMpae2TgZta4wjWCzF2xd84O3i1LE6ydVHcn4Wk5hvNQ4Qu9bLF5JA5IZkael8dXNIrm8OyNMHnZzR6fQM-PYUky1bcv15CVxmo1eponftfMfZ-F9jm-V18GCoWjuM5IT89Bqe_P8B-5Q:1ngFeG:2jRm1yzfUNVCwUC_dkgfvkSUBBlI08LcBUUVhWyTYRs', '2022-04-18 09:53:48.533676'),
('x1buetz8p72ym7iocaff7mjk3kehr3mj', '.eJyNVNtymzAQ_RUPz8ZGgATkMW0fOp1OPqDOMBJaGSWAKBKTdjL-97ImTczF4zwt7FnO2bNa8erlvHdl3lvoci29O49428uc4MUzNAjIJ94cza4wjeu02GHJ7g21u59GQnX_VjshKLkth6-plJAGccAZZERkWRFSplgc0hSiREQyUakIaUFVlhLKCpA0HhCVApUBJ5QiaQ1NbweuX68Hj7ftwbvbHLz67_lxOzw2vIYxeTj0SaEEBpAKg-AEQxywsVYPPsZaqHx88a3vjKnsCNfox2IBik2IYzgT03Bk_CAmc2bFN4r7OIUR6btqBKQwbv9kRC7BcV3tRxi0RJgEQXjabuayWUJDDCrLhpAKASirGFz1A87p5rgm_XspGZ0eMZG_gD6WDpPBtIDMe_p6b9wnO_FLqNqbg6WRTNEhKdQa79QC_GlN5_bXJdvO1MZpBKZG6MpwaZzI89FCfFv6xXTSXlWW3MFcks0lvzw8_Pj-bW0vp1KFMc8armoNm18txJIVf8Pu4GhZGCW3RXFhF5LzXf7QSxebQ6YF8djQ-43Ff8PKhWVJHOHxMxqcz4CPR1GsXCv7vxfflhoqeXO1Lv1_9pJim5ezeDdEgoXjcOo4Oz16p382f6LT:1nfNxy:-6WDpXPAR0lpFN6qnNMVoKcRYEIt3tEDvwNY0gRHsaY', '2022-04-16 00:34:34.268428');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `auth_group`
--
ALTER TABLE `auth_group`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indexes for table `auth_group_permissions`
--
ALTER TABLE `auth_group_permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `auth_group_permissions_group_id_permission_id_0cd325b0_uniq` (`group_id`,`permission_id`),
  ADD KEY `auth_group_permissio_permission_id_84c5c92e_fk_auth_perm` (`permission_id`);

--
-- Indexes for table `auth_permission`
--
ALTER TABLE `auth_permission`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `auth_permission_content_type_id_codename_01ab375a_uniq` (`content_type_id`,`codename`);

--
-- Indexes for table `auth_user`
--
ALTER TABLE `auth_user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- Indexes for table `auth_user_groups`
--
ALTER TABLE `auth_user_groups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `auth_user_groups_user_id_group_id_94350c0c_uniq` (`user_id`,`group_id`),
  ADD KEY `auth_user_groups_group_id_97559544_fk_auth_group_id` (`group_id`);

--
-- Indexes for table `auth_user_user_permissions`
--
ALTER TABLE `auth_user_user_permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `auth_user_user_permissions_user_id_permission_id_14a6b632_uniq` (`user_id`,`permission_id`),
  ADD KEY `auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm` (`permission_id`);

--
-- Indexes for table `dbot_config`
--
ALTER TABLE `dbot_config`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dbot_cookie`
--
ALTER TABLE `dbot_cookie`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dbot_export`
--
ALTER TABLE `dbot_export`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `export_export_uindex` (`export`),
  ADD UNIQUE KEY `export_id_uindex` (`id`);

--
-- Indexes for table `dbot_job_detail`
--
ALTER TABLE `dbot_job_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dbot_ql`
--
ALTER TABLE `dbot_ql`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ql_id_uindex` (`id`),
  ADD UNIQUE KEY `ql_url_uindex` (`url`);

--
-- Indexes for table `dbot_user`
--
ALTER TABLE `dbot_user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `dbot_user_id_uindex` (`id`);

--
-- Indexes for table `dbot_words`
--
ALTER TABLE `dbot_words`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `words_id_uindex` (`id`);

--
-- Indexes for table `django_admin_log`
--
ALTER TABLE `django_admin_log`
  ADD PRIMARY KEY (`id`),
  ADD KEY `django_admin_log_content_type_id_c4bce8eb_fk_django_co` (`content_type_id`),
  ADD KEY `django_admin_log_user_id_c564eba6_fk_auth_user_id` (`user_id`);

--
-- Indexes for table `django_content_type`
--
ALTER TABLE `django_content_type`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `django_content_type_app_label_model_76bd3d3b_uniq` (`app_label`,`model`);

--
-- Indexes for table `django_migrations`
--
ALTER TABLE `django_migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `django_session`
--
ALTER TABLE `django_session`
  ADD PRIMARY KEY (`session_key`),
  ADD KEY `django_session_expire_date_a5c62663` (`expire_date`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `auth_group`
--
ALTER TABLE `auth_group`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `auth_group_permissions`
--
ALTER TABLE `auth_group_permissions`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `auth_permission`
--
ALTER TABLE `auth_permission`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=57;
--
-- AUTO_INCREMENT for table `auth_user`
--
ALTER TABLE `auth_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `auth_user_groups`
--
ALTER TABLE `auth_user_groups`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `auth_user_user_permissions`
--
ALTER TABLE `auth_user_user_permissions`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=53;
--
-- AUTO_INCREMENT for table `dbot_config`
--
ALTER TABLE `dbot_config`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `dbot_cookie`
--
ALTER TABLE `dbot_cookie`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=89;
--
-- AUTO_INCREMENT for table `dbot_export`
--
ALTER TABLE `dbot_export`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `dbot_job_detail`
--
ALTER TABLE `dbot_job_detail`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `dbot_ql`
--
ALTER TABLE `dbot_ql`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `dbot_user`
--
ALTER TABLE `dbot_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `dbot_words`
--
ALTER TABLE `dbot_words`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `django_admin_log`
--
ALTER TABLE `django_admin_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `django_content_type`
--
ALTER TABLE `django_content_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `django_migrations`
--
ALTER TABLE `django_migrations`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=33;
--
-- 限制导出的表
--

--
-- 限制表 `auth_group_permissions`
--
ALTER TABLE `auth_group_permissions`
  ADD CONSTRAINT `auth_group_permissio_permission_id_84c5c92e_fk_auth_perm` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  ADD CONSTRAINT `auth_group_permissions_group_id_b120cbf9_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`);

--
-- 限制表 `auth_permission`
--
ALTER TABLE `auth_permission`
  ADD CONSTRAINT `auth_permission_content_type_id_2f476e4b_fk_django_co` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`);

--
-- 限制表 `auth_user_groups`
--
ALTER TABLE `auth_user_groups`
  ADD CONSTRAINT `auth_user_groups_group_id_97559544_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`),
  ADD CONSTRAINT `auth_user_groups_user_id_6a12ed8b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`);

--
-- 限制表 `auth_user_user_permissions`
--
ALTER TABLE `auth_user_user_permissions`
  ADD CONSTRAINT `auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  ADD CONSTRAINT `auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`);

--
-- 限制表 `django_admin_log`
--
ALTER TABLE `django_admin_log`
  ADD CONSTRAINT `django_admin_log_content_type_id_c4bce8eb_fk_django_co` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`),
  ADD CONSTRAINT `django_admin_log_user_id_c564eba6_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
